'use client'
import React from 'react';
import { Button, Checkbox, Form, Input, message } from 'antd';
import { signIn } from 'next-auth/react';
import { useRouter } from 'next/navigation';

type FieldType = {
    username?: string;
    password?: string;
    remember?: string;
};

const AuthSignIn = () => {
    const router = useRouter();

    const onFinish = async (values: any) => {

        const { username, password } = values;

        const res = await signIn("credentials", {
            username: username,
            password: password,
            redirect: false
        })
        if (!res?.error) {
            //redirect to home
            router.push("/");
        } else {
            message.error(res.error);
        }
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            style={{ maxWidth: 600 }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
        >
            <Form.Item<FieldType>

                label="Username"
                name="username"
                rules={[{ required: true, message: 'Please input your username!' }]}
            >
                <Input
                    placeholder='admin@gmail.com'
                />
            </Form.Item>

            <Form.Item<FieldType>
                label="Password"
                name="password"
                rules={[{ required: true, message: 'Please input your password!' }]}
            >
                <Input.Password
                    placeholder='123456'
                />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                <Button type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form>
    );
}




export default AuthSignIn;