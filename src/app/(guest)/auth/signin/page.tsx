import authOptions from "@/app/api/auth/auth.options"
import AuthSignIn from "./_components/auth.signin"
import { getServerSession } from "next-auth/next"
import { redirect } from 'next/navigation'


const SignInPage = async () => {
    const session = await getServerSession(authOptions);
    if (session) {
        // redirect to homepage
        redirect("/")
    }
    return (
        <AuthSignIn />
    )
}

export default SignInPage;